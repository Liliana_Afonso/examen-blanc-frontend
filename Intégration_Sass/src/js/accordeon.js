function tabsHandler() {
    console.log('La fonction tabshandler est lancée');

    var component = $('.tabs');

    component.each(function () {
        var that = $(this),
        componentNav = that.find('.tabs-nav--light'),
        componentNavItems = componentNav.find('li'),
        componentContents = that.find('.tabs-content');

        componentNavItems.on('click', function () {
            var targetID = $(this).data('id'); // this va prendre l'élément qu'on a cliqué
    
            console.log('Je clique sur ' + targetID); 
    
            if ($('#' + targetID).hasClass('article-inactive')) {
                console.log('Mon élément est masqué');
    
                // Gestion de l'affichage du contenu
                componentContents.addClass('article-inactive');
                $('#' + targetID).removeClass('article-inactive');
    
                // Gestion de l'affichage du menu
                componentNavItems.removeClass('tabs-nav-item--active');
                $(this).addClass('tabs-nav-item--active')
            } else {
                console.log('Mon élément est visible');
            }
        });
    });
};


$(document).ready(function () {
    var tabsComponent = $('.tabs');
    console.log('Le Dom est prêt');

    if (tabsComponent.length > 0) {
        console.log('Mon composant tabs est présent');
        tabsHandler();
    } else {
        console.log('Mon composant tabs n\'est pas chargé');
    }
});